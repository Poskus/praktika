<?php

namespace Drupal\posh_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Mano simple form class.
 */
class PoshForm extends FormBase{

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'posh_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['vardas'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Vardas'),
        ];

        $form['elektroninis_pastas'] = [
            '#type' => 'textfield',
            '#title' => $this->t('El.Paštas'),
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Pateikti formą'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function  submitForm(array &$form, FormStateInterface $form_state) {
        drupal_set_message($message = 'Forma pateikta. Laukite administratoriaus patvirtinimo!');
    }

}